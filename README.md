# Puppet Terraform Integration

This repository contains code and configurations for integrating Puppet and Terraform to automate the provisioning and configuration management of infrastructure components.

- Creating the instance via Terraform 
    - allowing traffic in port 22 for SSH connection
    - allowing traffic in port 8140 for puppet configuration

- configuring the puppet agent from installation 
    - with the help of provisioner

- writing manifests to acheive the required configuration as in server
