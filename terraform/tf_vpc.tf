provider "aws" {
  region = "ap-southeast-2" 

# Define Security Group
resource "aws_security_group" "puppet_sg" {
  name = "puppet_sg"

  ingress {
    from_port   = 8140
    to_port     = 8140
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] 
  }
}

# Define EC2 Instance
resource "aws_instance" "puppet_agent" {
  ami           = "ami-0d6f74b9139d26bf1" 
  instance_type = "t2.micro"              
  key_name      = "user3"                 

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y puppet-agent",
      "echo 'server = 192.168.29.99' | sudo tee -a /etc/puppetlabs/puppet/puppet.conf",
      "sudo /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true",
      "sudo /opt/puppetlabs/bin/puppet cert --waitforcert 60 --sign $(sudo /opt/puppetlabs/bin/puppet agent --fqdn)"
    ]

    connection {
      type        = "ssh"
      user        = "ubuntu" 
      private_key = file("user3.pem") 
      host = "self.public.ip"
    }
  }

  tags = {
    Name = "puppet_agent"
  }
}
